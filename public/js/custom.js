// window.ParsleyConfig = {

// };


$(document).ready(function() {
    NProgress.done();

    // Other function
    $('.select2').on('select2:unselect', function(event) {
        $el = $(this)
        $('.select2-search__field').val('');
        $(this).select2();
        $(this).focus();
    });

    $('.select2').on('select2:select', function(event) {
        $(this).focus();
    });
});

// Confirmation before delete for current and future element
$(document).on('click', '.js-delete-confirm', function(event) {
    event.preventDefault();
    var
        self = $(this);
        $form = self.closest('form');
        tempElement = $("<input type='hidden'/>");
        messages = self.attr('data-confirm') ? self.data('confirm') : "Are you sure to delete this resources?";

    console.log(messages);

    // clone the important parts of the button used to submit the form.
    tempElement
        .attr("name", self.data('name'))
        .attr("value", self.data('value'))
        .appendTo($form);
    bootbox.confirm({
        message: "<p class='m-t-sm'>"+messages+"</p>",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-danger'
            },
            cancel: {
                label: 'No',
                className: 'btn-default'
            }
        },
        callback: function(choice) {
            if (choice) {
                $form.submit();
            }
        }
    });
});
