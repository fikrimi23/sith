<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// -- Authentication Routes
Auth::routes();

// -- BEGIN -- public routes
Route::get('/', function () {
    return view('welcome');
});

Route::get('logout', function () {
    Auth::logout();
    session()->flush();
    return redirect('/');
});

Route::get('home', function () {
    return redirect()->route('dashboard.index');
});
// -- END -- Public routes

// -- BEGIN -- Model binding
    Route::model('activitylog', App\ActivityLog::class);
// -- END -- Model binding

// -- BEGIN -- Private route
Route::middleware('auth')->group(function () {
    // -- BEGIN -- Resource route
    Route::resource('dashboard', 'DashboardController', ['only' => ['index']]);
    Route::middleware('RoleAuth:App\User')
        ->resource('users', 'UsersController', ['except' => ['show']]);
    Route::middleware('RoleAuth:App\Module')
        ->resource('modules', 'ModulesController');
    Route::middleware('RoleAuth:App\Staff')
        ->resource('staffs', 'StaffsController');
    Route::middleware('RoleAuth:App\Rolegroup')
        ->resource('rolegroups', 'RolegroupsController', ['except' => ['show']]);
    Route::middleware('RoleAuth:App\Event')
        ->resource('events', 'EventsController');
    Route::middleware('RoleAuth:App\Activity')
        ->resource('activities', 'ActivitiesController');
    Route::middleware('RoleAuth:App\ActivityLog')
        ->resource('activitylogs', 'ActivityLogsController');
    Route::middleware('RoleAuth:App\Institute')
        ->resource('institutes', 'InstitutesController');
    Route::middleware('RoleAuth:App\Department')
        ->resource('departments', 'DepartmentsController');
    Route::middleware('RoleAuth:App\Calendar')
        ->resource('calendar', 'CalendarController');
    // -- END -- Resource binding

    // -- BEGIN -- Datatables Specific
    Route::group(['prefix' => 'datatables'], function () {
        Route::post('users', 'DatatablesController@users')
            ->name('datatables.users');
        Route::post('rolegroups', 'DatatablesController@rolegroups')
            ->name('datatables.rolegroups');
        Route::post('modules', 'DatatablesController@modules')
            ->name('datatables.modules');
        Route::post('activities', 'DatatablesController@activities')
            ->name('datatables.activities');
        // Route::post('staffs', 'StaffsController@getDatatablesResources')
        //     ->name('staffs.datatables');
        // Route::post('rolegroups', 'RolegroupsController@getDatatablesResources')
        //     ->name('rolegroups.datatables');
        // Route::post('institutes', 'InstitutesController@getDatatablesResources')
        //     ->name('institutes.datatables');
        // Route::post('departments', 'DepartmentsController@getDatatablesResources')
        //     ->name('departments.datatables');
    });
    // -- END -- Datatables Specific

    // -- BEGIN -- Other API Router
    Route::post('rolegroups/add_modules/{id}', 'RolegroupsController@addModules')
        ->name('rolegroups.addModules');
    Route::post('rolegroups/remove_modules/{id}', 'RolegroupsController@removeModules')
        ->name('rolegroups.removeModules');
    // -- END -- Other API Router
});
// -- END -- Model binding
