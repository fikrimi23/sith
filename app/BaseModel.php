<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Model;
use App\Traits\Uuids;

class BaseModel extends Model
{
    use Uuids;

    /**
     * Set incrementing to false because of UUID
     *
     * @var boolean
     */
    public $incrementing = false;

    /**
     * Hide sensitive attribute
     *
     * @var array
     */
    public $hidden = [
        'id', 'created_at', 'updated_at'
    ];

    /**
     * Default column parsed as date
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];
}
