<?php

namespace App\Helper;

class Flash
{
    public static function push($key, $value)
    {
        $values = session()->get($key, []);
        $values[] = $value;
        session()->flash($key, $values);
    }
}
