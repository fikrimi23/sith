<?php

namespace App;

use App\BaseModel as Model;

class Institute extends Model
{
    /**
     * Fillable column
     *
     * @var array
     */
    protected $fillable = [
        'title', 'name', 'type'
    ];

    /**
     * Appended column
     *
     * @var array
     */
    protected $appends = [
        'full_title'
    ];

    /**
     * Type id for every institutes in one periode
     * @var [type]
     */
    private static $types = [
        '1' => 'eksekutif',
        '2' => 'legislatif',
        '3' => 'yudikatif',
    ];

    /**
     * Make cooked types with [id] => 'type'
     *
     * @return array of types
     */
    public static function getCookedTypes()
    {
        $types = [];
        foreach (static::$types as $id => $type) {
            $types[$id] = "Lembaga ".ucfirst($type);
        }

        return $types;
    }

    /**
     * Get specific types based on id
     *
     * @param  integer $type_id type id
     * @return string        type
     */
    public static function getType($type_id)
    {
        return static::$types[$type_id];
    }

    /**
     * Attribute to show Intiials + cabinet name
     *
     * @return string
     */
    public function getFullTitleAttribute()
    {
        return getInitials($this->title)." ".$this->name;
    }

    /**
     * One-to-many relation to department
     *
     * @return \App\Department instance of department
     */
    public function departments()
    {
        return $this->hasMany('App\Department');
    }
}
