<?php

namespace App;

use App\BaseModel as Model;

class Department extends Model
{
    /**
     * Fillable column
     *
     * @var array
     */
    protected $fillable = [
        'title', 'institute_id', 'alias', 'description', 'vision', 'mission',
        ];

    /**
     * Many-to-one relation to institute
     *
     * @return \App\Institute instance of institute
     */
    public function institute()
    {
        return $this->belongsTo('App\Institute');
    }

    /**
     * Many-to-many relation to users
     *
     * @return \App\User instance of user
     */
    public function members()
    {
        return $this->belongsToMany('App\User')->withPivot('position');
    }
}
