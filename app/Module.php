<?php

namespace App;

use App\BaseModel as Model;

class Module extends Model
{
    protected $fillable = [
        'module_alias',
        'module_name',
        'module_core',
        'locked',
    ];

    /**
     * One-to-many relation to roles
     *
     * @return \App\Roles intance of Roles;
     */
    public function roles()
    {
        return $this->hasMany('App\Role');
    }
}
