<?php

namespace App;

use App\BaseModel as Model;

class Event extends Model
{
    /**
     * Fillable column
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'department_id',
        'description',
        'start_date',
        'end_date'
    ];

    /**
     * Overrided dates with start_date and end_date
     *
     * @var array
     */
    protected $dates = [
        'start_date',
        'end_date',
        'created_at',
        'updated_at',
    ];

    /**
     * Get unassigned event
     *
     * @return App\Event Event instance
     */
    public function scopeUnassigned()
    {
        return $this->whereNull('start_date');
    }
}
