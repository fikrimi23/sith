<?php

namespace App;

use App\BaseModel as Model;

class ActivityLog extends Model
{
    protected $fillable = [
        'activity_id',
        'user_id',
        'date',
        'title',
    ];

    protected $dates = [
        'date',
        'created_at',
        'updated_at',
    ];

    protected $guarded = [];

    public function activity()
    {
        return $this->belongsTo('\App\Activity');
    }

    public function user()
    {
        return $this->belongsTo('\App\User');
    }
}
