<?php

namespace App;

use App\BaseModel as Model;

class Activity extends Model
{
    protected $fillable = [
        "department_id",
        "title",
        "description",
        "is_event",
        "is_program",
    ];

    // protected $guarded = [];

    public function logs()
    {
        return $this->hasMany('\App\ActivityLog');
    }

    public function department()
    {
        return $this->belongsTo('\App\Department');
    }
}
