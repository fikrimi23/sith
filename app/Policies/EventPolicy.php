<?php

namespace App\Policies;

use App\User;

use Illuminate\Auth\Access\HandlesAuthorization;

class EventPolicy extends GlobalPolicy
{
    use HandlesAuthorization;

    public $roles;

    public function before(User $user, $ability)
    {
        $this->roles = $user->rolegroup->roles->where('module.module_alias', 'events');
    }
}
