<?php

namespace App\Policies;

use App\Dependencies\RoleAbilities;

class GlobalPolicy
{
    protected $abilities;

    public function __construct()
    {
        $this->abilities = [
            'READ'   => ['XREAD', 'READ'],
            'CREATE' => ['XCREATE', 'CREATE'],
            'UPDATE' => ['XUPDATE', 'UPDATE'],
            'DELETE' => ['XDELETE', 'DELETE'],
        ];
    }

    /**
     * Determine whether the user can view the users.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function view()
    {
        return $this->roles->whereIn('role_ability', $this->abilities['READ'])->count() > 0;
    }

    /**
     * Determine whether the user can create userss.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create()
    {
        return $this->roles->whereIn('role_ability', $this->abilities['CREATE'])->count() > 0;
    }

    /**
     * Determine whether the user can update the users.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function update()
    {
        return $this->roles->whereIn('role_ability', $this->abilities['UPDATE'])->count() > 0;
    }

    /**
     * Determine whether the user can delete the users.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function delete()
    {
        return $this->roles->whereIn('role_ability', $this->abilities['DELETE'])->count() > 0;
    }
}
