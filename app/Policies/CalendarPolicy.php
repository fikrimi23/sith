<?php

namespace App\Policies;

use App\User;

use Illuminate\Auth\Access\HandlesAuthorization;

class CalendarPolicy extends GlobalPolicy
{
    use HandlesAuthorization;

    public $roles;

    public function before(User $user, $ability)
    {
        $this->roles = $user->rolegroup->roles->where('module.module_alias', 'calendar');
    }
}
