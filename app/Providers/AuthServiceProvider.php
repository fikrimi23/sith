<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\BaseModel' => 'App\Policies\BaseModelPolicy',
        'App\User' => 'App\Policies\UserPolicy',
        'App\Rolegroup' => 'App\Policies\RolegroupPolicy',
        'App\Module' => 'App\Policies\ModulePolicy',
        'App\Institute' => 'App\Policies\InstitutePolicy',
        'App\Department' => 'App\Policies\DepartmentPolicy',
        'App\Activity' => 'App\Policies\ActivityPolicy',
        'App\ActivityLog' => 'App\Policies\ActivityLogPolicy',
        'App\Calendar' => 'App\Policies\CalendarPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
    }
}
