<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\UpdateModuleRequest;
use App\Module;

use App\Dependencies\CustomBuilder;

use Datatables;
use Form;
use DB;
use App\Helper\Flash;

class ModulesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(CustomBuilder $builder)
    {
        $html = $builder
            ->parameters()
            ->ajax([
                'url' => route('datatables.modules'),
            ])
            ->columns([
                ['data' => 'module_name',     'title' => trans('datatables.header.name')],
                ['data' => 'module_alias', 'title' => trans('datatables.header.alias')],
                ['data' => 'locked', 'title' => trans('datatables.header.locked')],
            ])
            ->addAction();
        return view('modules.index')->with(compact('html'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modules.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\UpdateModuleRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UpdateModuleRequest $request)
    {
        $module = DB::transaction(function () use ($request) {
            $module = Module::create($request->all());
            return $module;
        });
        return redirect()->route('modules.show', $module->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function show(Module $module)
    {
        return view('modules.show')->with('module', $module);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function edit(Module $module)
    {
        return view('modules.form')->with('module', $module);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\UpdateModuleRequest  $request
     * @param  \App\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateModuleRequest $request, Module $module)
    {
        $module = DB::transaction(function () use ($module, $request) {
            $module->update($request->except('module_alias'));
            return $module;
        });
        return redirect()->route('modules.show', $module->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function destroy(Module $module)
    {
        if ($module->locked) {
            Flash::push('error-message', trans('modules.delete_locked'));
            return redirect()->route('modules.index');
        }

        DB::transaction(function () use ($module) {
            $module->delete();
        });
        return redirect()->back();
    }
}
