<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;

use App\Rolegroup;
use App\Module;
use App\Role;
use App\Helper\Flash;
use App\Http\Requests\UpdateRolegroupRequest;

use App\Dependencies\CustomBuilder;

use Datatables;
use Form;
use DB;

class RolegroupsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(CustomBuilder $builder)
    {
        $html = $builder
            ->parameters()
            ->ajax([
                'url' => route('datatables.rolegroups'),
            ])
            ->columns([
                ['data' => 'rolegroup_name',     'title' => trans('datatables.header.name')],
                ['data' => 'members', 'title' => trans('datatables.header.members')],
                ['data' => 'level', 'title' => trans('datatables.header.level')],
            ])
            ->addAction();
        return view('rolegroups.index')->with(compact('html'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('rolegroups.form')->with([
            'modules' => Module::all()
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\UpdateRolegroupRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UpdateRolegroupRequest $request)
    {
        $rolegroup = DB::transaction(function () use ($request) {
            $rolegroup = Rolegroup::create($request->all());
            return $rolegroup;
        });

        return redirect()->route('rolegroups.edit', $rolegroup->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rolegroup $rolegroup
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rolegroup $rolegroup
     * @return \Illuminate\Http\Response
     */
    public function edit(Rolegroup $rolegroup)
    {
        $modules = Module::all()->pluck('module_name', 'id');
        $rolegroup->load(['roles', 'roles.module']);
        $abilities = [
            'XREAD' => 'XREAD',
            'XCREATE' => 'XCREATE',
            'XUPDATE' => 'XUPDATE',
            'XDELETE' => 'XDELETE',
            'READ' => 'READ',
            'CREATE' => 'CREATE',
            'UPDATE' => 'UPDATE',
            'DELETE' => 'DELETE',
        ];
        $roles = [];
        foreach ($rolegroup->roles as $role) {
            $roles[$role->module_id][] = $role;
        }
        $roles = collect($roles);

        return view('rolegroups.form')->with([
            'rolegroup' => $rolegroup,
            'modules' => $modules,
            'abilities' => $abilities,
            'rolesCooked' => $roles,
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateRolegroupRequest  $request
     * @param  \App\Rolegroup $rolegroup
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRolegroupRequest $request, Rolegroup $rolegroup)
    {
        DB::transaction(function () use ($request, $rolegroup) {
            $rolegroup->fill($request->only(['rolegroup_name', 'rolegroup_depth']));
            $rolegroup->save();

            Role::where('rolegroup_id', $rolegroup->id)->delete();
            foreach ($request->abilities as $module_id => $role_abilities) {
                foreach ($role_abilities as $role_ability) {
                    $role = new Role();
                    $role->fill([
                        'rolegroup_id' => $rolegroup->id,
                        'module_id' => $module_id,
                        'role_ability' => $role_ability,
                        ]);
                    $role->save();
                }
            }
        });
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rolegroup $rolegroup
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rolegroup $rolegroup)
    {
        DB::transaction(function () use ($rolegroup) {
            $rolegroup->delete();
        });
        return redirect()->route('rolegroups.index');
    }

    /**
     * Add modules to current rolegroup
     *
     * @param Request $request contain deleted module request
     * @param uuid  $id      Module ID
     */
    public function addModules(Request $request, $id)
    {
        $response = ['status' => false];

        $response = DB::transaction(function () use ($request, $id) {
            $roles = Role::all();
            Role::where('module_id', $request->input('module_id'))->where('rolegroup_id', $id)->delete();
            foreach ($request->input('module_id') as $module_id) {
                $role = new Role([
                    'rolegroup_id' => $id,
                    'module_id' => $module_id,
                    'role_ability' => 'READ'
                    ]);
                $role->save();
            }

            Flash::push('success-message', 'Module has been added');
            return ['status' => true];
        });

        return collect($response);
    }

    /**
     * Remove modules from current rolegroup
     *
     * @param Request $request contain request
     * @param uuid  $id      Module ID
     */
    public function removeModules(Request $request, $id)
    {
        $response = ['status' => false];

        $response = DB::transaction(function () use ($request, $id) {
            Role::where('rolegroup_id', $id)->where('module_id', $request->input('module_id'))->delete();
            return ['status' => true];
        });
        return collect($response);
    }
}
