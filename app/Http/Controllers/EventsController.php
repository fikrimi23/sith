<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// use App\Http\Requests\UpdateModuleRequest;
use App\Event;

use Datatables;
use DB;
use App\Helper\Flash;

class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::whereNotNull('start_date')->get();

        $cookedEvents = [];
        foreach ($events as $event) {
            $tmp = [
                'id' => $event->id,
                'title' => $event->title,
                'start' => $event->start_date->format('Y-m-d')
            ];

            $tmp['end'] = $event->end_date ? $event->end_date->format('Y-m-d') : null;

            if ($event->start_date->format('H:i:s') == '00:00:00') {
                $tmp['allDay'] = true;
            }

            $cookedEvents[] = $tmp;
        }

        return $cookedEvents;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('events.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $event = DB::transaction(function () use ($request) {
            $startDate = new \DateTime($request->input('start'));
            $endDate = new \DateTime($request->input('end'));

            $event = Event::create([
                'title' => $request->input('title'),
                'department_id' => $request->input('department_id'),
                'description' => 'dummy',
                'start_date' => $request->input('start'),
                'end_date' => $request->input('end'),
            ]);

            return $event;
        });
        return collect([
            'id' => $event->id,
            'title' => $event->title,
            'start' => $event->start_date->format('Y-m-d'),
            'end' => $event->end_date->format('Y-m-d'),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event)
    {
        DB::transaction(function () use ($request, $event) {
            $startDate = new \DateTime($request->input('start'));
            $endDate = new \DateTime($request->input('end'));

            $event->update([
                'title' => $request->input('title') ?: $event->title,
                'department_id' => $request->input('department_id') ?: $event->department_id,
                'description' => 'dummy',
                'start_date' => $request->input('start') ?: null,
                'end_date' => $request->input('end') ?: null,
            ]);
        });
        return collect($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Event $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        DB::transaction(function () use ($event) {
            $event->delete();
        });

        return collect(['status' => true]);
    }

    /* BEGIN custom function */

    /**
     * Get resources for datatables
     *
     * @return JSON Datatables JSON data
     */
    public function getDatatablesResources()
    {
        return Datatables::of(Event::get([
            'id', 'title', 'start_date', 'end_date'
            ]))
            ->addColumn('action', function ($event) {
                $settings = [
                    'title' => 'events',
                    'module' => $event,
                    'part' => [
                        'view' => true,
                        'edit' => true,
                        'delete' => true,
                    ]
                ];
                $html = view('layouts.datatablesActionColumn')->with($settings)->render();

                return $html;
            })
            ->removeColumn('id')
            ->make(true);
    }
}
