<?php

namespace App\Http\Controllers;

use App\Department;
use App\Institute;
use App\Http\Requests\UpdateDepartmentRequest;
use Illuminate\Http\Request;

use DB;

class DepartmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            if ($request->input('action') == 'all_by_institutes') {
                return Department::where('institute_id', $request->input('institute_id'))->get()->makeVisible('id');
            }
            if ($request->input('action') == 'institutes_dropdown') {
                return Department::where('institute_id', $request->input('institute_id'))->pluck('title', 'id');
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $institutes = Institute::all();

        return view('departments.form')->with([
            'institutes' => $institutes->pluck('full_title', 'id'),
            // make dummy department
            'department' => new Department(['institute_id' => $request->input('institute_id')]),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\UpdateDepartmentRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UpdateDepartmentRequest $request)
    {
        $department = DB::transaction(function () use ($request) {
            $department = new Department($request->all());
            $department->institute_id = $request->input('institute.id');
            $department->save();
            return $department;
        });
        return redirect()->route('institutes.show', $department->institute_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function show(Department $department)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function edit(Department $department)
    {
        $institutes = Institute::all();
        return view('departments.form')->with([
            'department' => $department,
            'institutes' => $institutes->pluck('full_title', 'id'),
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateDepartmentRequest  $request
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDepartmentRequest $request, Department $department)
    {
        $department = DB::transaction(function () use ($request, $department) {
            $department->fill($request->all());
            $department->save();
            return $department;
        });
        return redirect()->route('departments.edit', $department->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function destroy(Department $department)
    {
        $institute_id = $department->institute_id;
        DB::transaction(function () use ($department) {
            $department->delete();
        });
        return redirect()->route('institutes.show', $institute_id);
    }
}
