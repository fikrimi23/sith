<?php

namespace App\Http\Controllers;

use App\Activity;
use App\Institute;
use App\Department;

use Illuminate\Http\Request;
use App\Dependencies\CustomBuilder;

use DB;

class ActivitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(CustomBuilder $builder)
    {
        $html = $builder
            ->parameters()
            ->ajax([
                'url' => route('datatables.activities'),
            ])
            ->columns([
                ['data' => 'title',             'title' => trans('datatables.header.title')],
                ['data' => 'department.title',     'title' => trans('datatables.header.department')],
            ])
            ->addAction();
        return view('activities.index')->with(compact('html'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $institutes = Institute::all()->pluck('fulltitle', 'id');

        $institutes->prepend('Please Choose Institutes', '');

        return view('activities.form')->with([
            'institutes' => $institutes
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $activity = DB::transaction(function () use ($request) {
            $activity = new Activity($request->all());
            $activity->department_id = $request->input('department.id');
            $activity->save();
            return $activity;
        });
        return redirect()->route('activities.show', $activity->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function show(Activity $activity)
    {
        return view('activities.show')->with([
            'activity' => $activity,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function edit(Activity $activity)
    {
        $institutes = Institute::all()->pluck('fulltitle', 'id');
        $departments = Department::all()->pluck('title', 'id');

        $institutes->prepend('Please Choose Institutes', '');
        $departments->prepend('Please Choose Institutes', '');

        return view('activities.form')->with([
            'activity' => $activity,
            'institutes' => $institutes,
            'departments' => $departments,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Activity $activity)
    {
        $activity = DB::transaction(function () use ($activity, $request) {
            $activity->update($request->all());
            return $activity;
        });
        return redirect()->route('activities.show', $activity->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function destroy(Activity $activity)
    {
        DB::transaction(function () use ($activity) {
            $activity->delete();
        });
        return redirect()->back();
    }
}
