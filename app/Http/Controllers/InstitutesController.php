<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Institute;
use App\Http\Requests\UpdateInstituteRequest;

use Datatables;
use DB;
use Flash;

class InstitutesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $institutes = Institute::with('departments')->orderBy('type')->get();
        return view('institutes.index')->with('institutes', $institutes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('institutes.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UpdateInstituteRequest $request)
    {
        $institute = DB::transaction(function () use ($request) {
            $institute = Institute::create($request->all());

            // TODO : Lock core
            $institute->departments()->create([
                'title' => 'core',
                'alias' => 'core',
                'description' => 'core',
                'vision' => 'core',
                'mission' => 'core',
            ]);

            return $institute;
        });
        return redirect()->route('institutes.show', $institute->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Institute  $institute
     * @return \Illuminate\Http\Response
     */
    public function show(Institute $institute)
    {
        return view('institutes.show')->with(['institute' => $institute]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Institute  $institute
     * @return \Illuminate\Http\Response
     */
    public function edit(Institute $institute)
    {
        return view('institutes.form')->with(['institute' => $institute]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests  $request
     * @param  \App\Institute  $institute
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateInstituteRequest $request, Institute $institute)
    {
        $institute = DB::transaction(function () use ($request, $institute) {
            $institute->update($request->all());
            return $institute;
        });
        return redirect()->route('institutes.show', $institute->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Institute  $institute
     * @return \Illuminate\Http\Response
     */
    public function destroy(Institute $institute)
    {
        DB::transaction(function () use ($institute) {
            $institute->delete();
        });
        return redirect()->route('institutes.index');
    }

    // -- BEGIN -- Custom Function

    /**
     * AJAX datatables resources handler
     *
     * @return JSON Datatable Ready JSON
     */
    public function getDatatablesResources()
    {
        if (\App\Helper\AuthHelper::isXUser('READ', 'institutes')) {
            $institutes = new Institute();
        } else {
            $institutes = Institute::where('rolegroup_depth', '>', '0');
        }

        return Datatables::of($institutes->with(['departments', 'departments.members'])->get([
            'id', 'title', 'name'
            ]))
            ->addColumn('members', function ($institute) {
                $members = 0;
                $institute->departments->each(function ($department) use (&$members) {
                    $members += $department->members()->count();
                });
                return $members;
            })
            ->addColumn('action', function ($institute) {
                $settings = [
                    'title' => 'institutes',
                    'module' => $institute,
                    'part' => [
                        'view' => $this->authorize('view', $institute),
                        'edit' => $this->authorize('edit', $institute),
                        'delete' => $this->authorize('delete', $institute),
                    ]
                ];
                $html = view('layouts.datatablesActionColumn')->with($settings)->render();
                return $html;
            })
            ->removeColumn('id')
            ->make(true);
    }
}
