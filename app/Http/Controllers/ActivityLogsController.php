<?php

namespace App\Http\Controllers;

use App\ActivityLog;

use Illuminate\Http\Request;

use DB;

class ActivityLogsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $activity = DB::transaction(function () use ($request) {
            $activity = new ActivityLog($request->all());
            // $activity->department_id = $request->input('department.id');
            $activity->save();
            return $activity;
        });
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ActivityLog  $log
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ActivityLog $log)
    {
        $log = DB::transaction(function () use ($log, $request) {
            $log->update($request->all());
            return $log;
        });
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ActivityLog  $log
     * @return \Illuminate\Http\Response
     */
    public function destroy(ActivityLog $log)
    {
        DB::transaction(function () use ($log) {
            $log->delete();
        });
        return redirect()->back();
    }
}
