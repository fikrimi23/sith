<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Datatables;
use Form;

class DatatablesController extends Controller
{
    /**
     * POST Controler for getting User datatable
     *
     * @return JSON Datatables JSON data
     */
    public function users()
    {
        // check for auth
        $this->authorize('view', 'App\User');

        // get collection
        $users = \App\User::get([
            'id', 'email', 'is_active'
        ]);

        // return datatables
        return Datatables::of($users)
            ->editColumn('is_active', function ($user) {
                if ($user->is_active) {
                    return '<span class="badge badge-success">Active</span>';
                } else {
                    return '<span class="badge badge-danger">Not Active</span>';
                }
            })
            ->addColumn('action', function ($user) {
                $url = [
                    'edit' => route('users.edit', $user->id),
                    'delete' => route('users.destroy', $user->id),
                ];
                return view('layouts.datatablesActionColumn')->with($url)->render();
            })
            ->removeColumn('id')
            ->make(true);
    }

    /**
     * POST Controler for getting rolegroups datatable
     *
     * @return JSON Datatables JSON data
     */
    public function rolegroups()
    {
        // check for auth
        $this->authorize('view', 'App\Rolegroup');

        // get collection
        if (\App\Helper\AuthHelper::isXUser('READ', 'rolegroups')) {
            $rolegroups = new \App\Rolegroup();
        } else {
            $rolegroups = \App\Rolegroup::where('rolegroup_depth', '>', '0');
        }

        $rolegroups = $rolegroups->orderBy('rolegroup_depth')->get([
            'id', 'rolegroup_name', 'rolegroup_depth'
            ]);

        // return datatables
        return Datatables::of($rolegroups)
            ->addColumn('members', function ($rolegroup) {
                return $rolegroup->users()->count();
            })
            ->addColumn('level', function ($rolegroup) {
                return $rolegroup->rolegroup_depth;
            })
            ->addColumn('action', function ($rolegroup) {
                $url = [
                    'edit' => route('rolegroups.edit', $rolegroup->id),
                    'delete' => route('rolegroups.destroy', $rolegroup->id),
                ];
                return view('layouts.datatablesActionColumn')->with($url)->render();
            })
            ->removeColumn('id')
            ->make(true);
    }

    /**
     * POST Controler for getting modules datatable
     *
     * @return JSON Datatables JSON data
     */
    public function modules()
    {
        // check for auth
        $this->authorize('view', 'App\Module');

        // get collection
        $modules = \App\Module::orderBy('module_alias')->get([
            'id', 'module_alias', 'module_name', 'locked'
            ]);

        // return datatables
        return Datatables::of($modules)
            ->editColumn('locked', function ($module) {
                if ($module->locked) {
                    return '<span class="badge badge-danger">locked</span>';
                } else {
                    return '<span class="badge badge-success">unlocked</span>';
                }
            })
            ->addColumn('action', function ($module) {
                $url = [
                    'view' => route('modules.show', $module->id),
                    'edit' => route('modules.edit', $module->id),
                    'delete' => route('modules.destroy', $module->id),
                ];
                return view('layouts.datatablesActionColumn')->with($url)->render();
            })
            ->removeColumn('id')
            ->make(true);
    }


    /**
     * POST Controler for getting events datatable
     *
     * @return JSON Datatables JSON data
     */
    public function activities()
    {
        // check for auth
        $this->authorize('view', 'App\Activity');

        // get collection
        $activities = \App\Activity::with('department')->get([
            'id', 'department_id', 'title'
        ]);

        // return datatables
        return Datatables::of($activities)
            ->addColumn('action', function ($module) {
                $url = [
                    'view' => route('activities.show', $module->id),
                    'edit' => route('activities.edit', $module->id),
                    'delete' => route('activities.destroy', $module->id),
                ];
                return view('layouts.datatablesActionColumn')->with($url)->render();
            })
            ->editColumn('department.title', function ($module) {
                $module->load('department.institute');
                if ($module->department->title == 'core') {
                    return $module->department->institute->full_title;
                }
                return $module->department->title;
            })
            ->removeColumn('id')
            ->make(true);
    }
}
