<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use Request;
use App\Role;

class NavbarComposer
{
    private function menu()
    {
        $user = auth()->user();

        $menu = [
            // -- BEGIN -- Dashboard
            [
                'icon' => 'fa-tachometer',
                'title' => trans('navbar.dashboard'),
                'url' => route('dashboard.index'),
                'active' => Request::is('dashboard'),
            ],
            // -- END -- Dashboard
            // -- BEGIN -- Account
            [
                'icon' => 'fa-users',
                'title' => trans('navbar.users.title'),
                'url' => '#',
                'authorized' =>
                    $user->can('view', 'App\User')
                    || $user->can('view', 'App\Rolegroup')
                    ,
                'active' =>
                    Request::is('staffs*') ||
                    Request::is('users*') ||
                    Request::is('rolegroups*'),
                'subnav' => [
                    [
                        'icon' => 'fa-user',
                        'url' => route('staffs.index'),
                        'authorized'  => $user->can('view', 'App\Staff'),
                        'title' => trans('navbar.users.staffs'),
                        'active' => Request::is('staffs*'),
                    ],
                    [
                        'icon' => 'fa-user',
                        'url' => route('users.index'),
                        'authorized'  => $user->can('view', 'App\User'),
                        'title' => trans('navbar.users.users'),
                        'active' => Request::is('users*'),
                    ],
                    [
                        'icon' => 'fa-user-plus',
                        'url' => route('rolegroups.index'),
                        'authorized'  => $user->can('view', 'App\Rolegroup'),
                        'title' => trans('navbar.users.rolegroups'),
                        'active' => Request::is('rolegroups*'),
                    ],
                ],
            ],
            // -- END -- Account
            // -- Begin -- Activity
            [
                'icon' => 'fa-file',
                'title' => trans('navbar.activities'),
                'url' => route('activities.index'),
                'active' => Request::is('activities'),
                'authorized' => $user->can('view', 'App\Activity'),
            ],
            // -- Begin -- Calendar
            [
                'icon' => 'fa-calendar',
                'title' => trans('navbar.calendar'),
                'url' => route('calendar.index'),
                'active' => Request::is('calendar'),
                'authorized' => $user->can('view', 'App\Calendar'),
            ],
            // -- END -- Calendar
            // -- Begin -- Organization
            [
                'icon' => 'fa-sitemap',
                'title' => trans('navbar.organizations.title'),
                'url' => route('institutes.index'),
                'active' => Request::is('institutes*')
                            // || Request::is('departments*')
                            ,
                'authorized'  =>
                    $user->can('view', 'App\Institute')
                    // || $user->can('view', 'App\Department')
                    ,
                // 'subnav'  => [
                //     [
                //         'icon'    => 'fa-sitemap',
                //         'title' => trans('navbar.organizations.institutes'),
                //         'url' => route('institutes.index'),
                //         'active' => Request::is('institutes*'),
                //         'authorized'  => $user->can('view', 'App\Institute'),
                //     ],
                //     [
                //         'icon'    => 'fa-sitemap',
                //         'title' => trans('navbar.organizations.departments'),
                //         'url' => route('departments.index'),
                //         'active' => Request::is('departments*'),
                //         'authorized'  => $user->can('view', 'App\Department'),
                //     ],
                // ],
            ],
            // -- END -- Organization
            // -- BEGIN -- Module
            [
                'icon'    => 'fa-cogs',
                'title'   => trans('navbar.settings.title'),
                'url'     => "#",
                'authorized'  => $user->can('view', 'App\Module'),
                'active' => Request::is('modules*'),
                'subnav'  => [
                    [
                        'icon'    => 'fa-puzzle-piece',
                        'url'     => route('modules.index'),
                        'authorized'  => $user->can('view', 'App\Module'),
                        'title'   => trans('navbar.settings.modules'),
                        'active' => Request::is('modules*'),
                    ]
                ]
            ],
            // -- END -- Module
        ];

        return $menu;
    }

    public function compose(View $view)
    {
        $html_test = view('layouts.navbar')->with('menu', $this->menu())->render();

        $view->with('navbar', $html_test);
    }
}
