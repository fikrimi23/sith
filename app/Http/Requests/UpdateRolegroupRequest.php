<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRolegroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $generalRule = [
            'rolegroup_name' => 'required|max:255',
            'rolegroup_depth' => 'required|min:1|max:99'
        ];

        $specificRule = [];
        switch ($this->method()) {
            case 'POST':
                // $specificRule = [
                //     'module_alias' => 'alpha|max:255|unique:modules,module_alias'
                // ];
                break;
            case 'PUT':
                // $specificRule = [
                //     'module_alias' => 'alpha|max:255|unique:modules,module_alias,'.$this->segment(2)
                // ];
                break;
        }
        return array_merge($generalRule, $specificRule);
    }

    /**
     * Custom messages
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required' => 'A title is required',
            'body.required'  => 'A message is required',
        ];
    }
}
