<?php

namespace App\Dependencies;

use Yajra\Datatables\Html\Builder;

/**
 * This will make default parameter and language for databable
 */
class CustomBuilder extends Builder
{
    /**
     * Add default parameters
     *
     * @todo Still must be invoked manually
     * @param  array  $attributes default attributes
     * @return Yajra\Datatables\Html\Builder             Builder instance
     */
    public function parameters(array $attributes = [])
    {
        // default parameters
        $default = [
            'searchDelay' => 350,
            'language' => [
                "lengthMenu"     => trans('datatables.lengthMenu'),
                "zeroRecords"    => trans('datatables.zeroRecords'),
                "info"           => trans('datatables.info'),
                "infoEmpty"      => trans('datatables.infoEmpty'),
                "infoFiltered"   => trans('datatables.infoFiltered'),
                "emptyTable"     => trans('datatables.emptyTable'),
                "loadingRecords" => trans('datatables.loadingRecords'),
                "processing"     => trans('datatables.processing'),
                "search"         => trans('datatables.search'),

                "paginate" => [
                    "first"    => trans('datatables.paginate.first'),
                    "last"     => trans('datatables.paginate.last'),
                    "next"     => trans('datatables.paginate.next'),
                    "previous" => trans('datatables.paginate.previous')
                ],
            ],
        ];

        // if there's custom attributes passed to ths function, merge with that
        $this->attributes = array_merge($this->attributes, $default, $attributes);

        return $this;
    }

    /**
     * Add default ajax
     *
     * @param  array $attributes array of ajax attribtues
     * @return \Yajra\Datatables\Html\Builder             Builder instance
     */
    public function ajax($attributes)
    {
        $default = [
            'type' => 'POST',
            'headers' => [
                'X-CSRF-TOKEN' => csrf_token(),
            ]
        ];

        // if there's custom attributes passed to ths function, merge with that
        $this->ajax = array_merge($default, $attributes);

        return $this;
    }
}
