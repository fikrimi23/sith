<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepartmentsRelationship extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('departments')) {
            Schema::table('departments', function ($table) {
                $table->foreign('institute_id')
                    ->references('id')
                    ->on('institutes')
                    ->onDelete('CASCADE');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('departments')) {
            Schema::table('departments', function ($table) {
                $table->dropForeign(['institute_id']);
            });
        }
    }
}
