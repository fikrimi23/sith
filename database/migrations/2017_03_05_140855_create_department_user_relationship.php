<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepartmentUserRelationship extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('department_user')) {
            Schema::table('department_user', function($table) {
                $table->foreign('department_id')
                    ->references('id')
                    ->on('departments')
                    ->onDelete('CASCADE');
                $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('CASCADE');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('department_user')) {
            Schema::table('department_user', function($table) {
                $table->dropForeign(['department_id']);
                $table->dropForeign(['user_id']);
            });
        }
    }
}
