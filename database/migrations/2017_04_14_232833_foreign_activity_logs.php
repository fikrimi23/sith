<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignActivityLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('activity_logs')) {
            Schema::table('activity_logs', function ($table) {
                $table->foreign('activity_id')
                    ->references('id')
                    ->on('activities')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
                $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('activity_logs')) {
            Schema::table('activity_logs', function ($table) {
                $table->dropForeign(['activity_id']);
                $table->dropForeign(['user_id']);
            });
        }
    }
}
