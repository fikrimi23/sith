<?php

use Illuminate\Database\Seeder;

class ModulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = DB::table('modules');
        $table->delete();

        $records = [
            [
                'module_alias' => 'dashboard',
                'module_name'  => 'Dashboard',
                'module_core'  => 1,
                'locked'       => 1,
            ],
            [
                'module_alias'  => 'users',
                'module_name'   => 'Users Management',
                'module_core'   => 1,
                'locked'        => 1,
            ],
            [
                'module_alias'  => 'modules',
                'module_name'   => 'Modules Management',
                'module_core'   => 1,
                'locked'        => 1,
            ],
            [
                'module_alias'  => 'rolegroups',
                'module_name'   => 'Role Groups',
                'module_core'   => 1,
                'locked'        => 1,
            ]
        ];

        $spec_record = [
            [
                'module_alias' => 'institutes',
                'module_name'  => 'Institutes Management',
                'module_core'  => 1,
                'locked'       => 1,
            ],
            [
                'module_alias' => 'departments',
                'module_name'  => 'Departments Management',
                'module_core'  => 1,
                'locked'       => 1,
            ],
            [
                'module_alias' => 'events',
                'module_name'  => 'Events Management',
                'module_core'  => 1,
                'locked'       => 1,
            ],
            [
                'module_alias' => 'calendar',
                'module_name'  => 'Calendar Management',
                'module_core'  => 1,
                'locked'       => 1,
            ],
        ];

        collect(array_merge($records, $spec_record))->each(function ($record) {
            App\Module::create(array_merge($record));
        });
    }
}
