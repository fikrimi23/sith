<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = DB::table('roles');
        $table->delete();

        $rolegroup = App\Rolegroup::where('rolegroup_depth', 0)->first();

        $modules = [
            'dashboard' => App\Module::where('module_alias', 'dashboard')->first(),
            'users' => App\Module::where('module_alias', 'users')->first(),
            'modules' => App\Module::where('module_alias', 'modules')->first(),
            'rolegroups' => App\Module::where('module_alias', 'rolegroups')->first(),

            'institutes' => App\Module::where('module_alias', 'institutes')->first(),
            'departments' => App\Module::where('module_alias', 'departments')->first(),
            'events' => App\Module::where('module_alias', 'events')->first(),
            'calendar' => App\Module::where('module_alias', 'calendar')->first(),
        ];

        $roles_name = ['XREAD', 'XUPDATE', 'XCREATE', 'XDELETE'];

        $records = [];
        foreach ($modules as $module_name => $module) {
            $tmp_roles = [];
            foreach ($roles_name as $role_name) {
                $tmp_roles[] = [
                    'rolegroup_id'  => $rolegroup->id,
                    'module_id'     => $module->id,
                    'role_ability'  => $role_name,
                ];
            }
            $records = array_merge_recursive($tmp_roles, $records);
        }

        collect($records)->each(function ($record) {
            App\Role::create($record);
        });
    }
}
