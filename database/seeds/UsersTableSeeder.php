<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = DB::table('users');
        $table->delete();

        $records = [
            [
                'rolegroup_id'  => App\Rolegroup::where('rolegroup_depth', 0)->first()->id,
                'email'         => 'super.admin@email.com',
                'password'      => Hash::make('hakamori'),
                'is_active'     => 1,
            ],
            [
                'rolegroup_id'  => App\Rolegroup::where('rolegroup_depth', 1)->first()->id,
                'email'         => 'general.admin@email.com',
                'password'      => Hash::make('hakamori'),
                'is_active'     => 1,
            ]
        ];

        collect($records)->each(function ($record) {
            App\User::create($record);
        });
    }
}
