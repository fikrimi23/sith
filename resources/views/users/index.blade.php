@extends('layouts.dashboard')

@section('css')
    {!! Layouts::usePlugin('datatables', 'css') !!}
@endsection

@section('content')
    @component('layouts.components.datatables')
        @slot('title')
            @choice('users.title', 2)
        @endslot

        @slot('topButton')
            @can('create', 'App\User')
                <a href="{{ route('users.create') }}" class="btn btn-primary btn-sm pull-right">
                    <i class="fa fa-fw fa-user"></i> New @choice('users.title', 1)
                </a>
            @endcan
        @endslot

        @slot('table')
            {!! $html->table(['style' => 'width:100%;']) !!}
        @endslot

    @endcomponent
@endsection

@push('js')
    {!! Layouts::usePlugin('datatables', 'js') !!}
    {!! $html->scripts() !!}
@endpush
