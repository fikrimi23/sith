@extends('layouts.dashboard')

@section('css')
@endsection

@section('breadcrumbs')
    <li class="breadcrumb-item"><a href="{{ route('institutes.index') }}">Institute</a></li>
    <li class="breadcrumb-item"><a href="{{ route('institutes.show', $institute->id) }}">{{ getInitials($institute->title)." ".$institute->name }}</a></li>
@endsection

@section('content')
<style type="text/css">
    .dept_profile {
        margin: auto;
        width: 100px;
        height: 100px;
    }
    .dept_name {
        text-align: center;
    }

    p {
        overflow: auto;
    }

    .count {
        text-align: center
    }
</style>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class=x_panel>
                <div class=x_content>
                    <div class="row">
                        <div class="col-xs-10">
                            <p class="m-b-xs">Lembaga {{ ucfirst(\App\Institute::getType($institute->type)) }}</p>
                            <h3><span class="text-uppercase">{{ $institute->name }}</span> <small>({{ ucwords($institute->title) }})</small></h3>
                        </div>
                        <div class="col-xs-2">
                        @if (auth()->user()->can('update', $institute) or auth()->user()->can('delete', $institute))
                            {!! Form::open(['url' => route('institutes.destroy', $institute->id), 'method'=>'delete' ,]) !!}
                                @can('update', $institute)
                                    <a href="{{ route('institutes.edit', $institute->id) }}" class="btn btn-block btn-primary"><i class="fa fa-fw fa-edit"></i> Edit</a>
                                @endcan
                                @can('delete', $institute)
                                    <a href="#" class="btn btn-block btn-danger js-delete-confirm"><i class="fa fa-fw fa-trash"></i> Delete</a>
                                @endcan
                            {!! Form::close() !!}
                        @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        @foreach ($institute->departments as $department)
            <div class="col-md-3 col-xs-12 widget widget_tally_box">
                <div class="x_panel">
                    <div class="x_content">
                        @if ($department->profile_img !== null)
                            <div class="dept_profile">
                                <img src="{{ asset('images/user.png') }}" alt="..." class="img-circle profile_img">
                            </div>
                        @endif
                        <h3 class="dept_name">{{ $department->alias }}</h3>
                        <div class="row">
                            <div class="col-xs-12 text-center">
                                @can('view', $department)
                                    <a class="btn btn-dark btn-xs" href="{{ route('departments.show', $department->id) }}"><i class="fa fa-eye"></i> View </a>
                                @endcan
                                @can('update', $department)
                                    <a class="btn btn-primary btn-xs" href="{{ route('departments.edit', $department->id) }}"><i class="fa fa-edit"></i> Edit </a>
                                @endcan
                            </div>
                        </div>
                        <div class="line"></div>
                        <div class="row">
                            <div class="col-xs-6 count">
                                <h3>{{ $department->members->count() }}</h3>
                                <span>Members</span>
                            </div>
                            <div class="col-xs-6 count">
                                <h3>1234</h3>
                                <span>Programs</span>
                            </div>
                        </div>
                        <div class="line"></div>
                        <div class="row">
                            <div class="col-xs-12">
                                <p>
                                    {{ $department->description }}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        <div class="animated flipInY col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <a href="{{ route('departments.create', ['institute_id' => $institute->id]) }}">
                <div class="tile">
                    <div class="icon"><i class="fa fa-plus"></i>
                    </div>
                </div>
            </a>
        </div>
    </div>
</section>
@endsection

@push('js')
@endpush
