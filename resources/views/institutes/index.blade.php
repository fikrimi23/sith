@extends('layouts.dashboard')

@section('breadcrumbs')

@endsection

@section('content')
    <div class="page-title">
        <div class="title_left">
            <h3>@choice('institutes'.'.title', 2) <small>(click to view)</small></h3>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        @foreach ($institutes as $institute)
        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a href="{{ route('institutes.show', $institute->id) }}">
                <div class="tile-stats">
                    @php
                        switch ($institute->type) {
                            case '1':
                                $icon = 'fa-users';
                                break;
                            case '2':
                                $icon = 'fa-file-text-o';
                                break;
                            case '3':
                                $icon = 'fa-university';
                                break;

                            default:
                                $icon = 'fa-sitemap';
                                break;
                        }
                    @endphp
                    <div class="icon"><i class="fa {{ $icon }}"></i>
                    </div>
                    <div class="count">{{ $institute->departments->count() }} <span class="small-tile-stats-font">dept</span></div>

                    <h3>{{ $institute->full_title }}</h3>
                    <p>Lorem ipsum psdea itgum rixt.</p>
                </div>
            </a>
        </div>
        @endforeach
        <div class="animated flipInY col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <a href="{{ route('institutes.create') }}">
                <div class="tile">
                    <div class="icon"><i class="fa fa-plus"></i>
                    </div>
                </div>
            </a>
        </div>
    </div>
@stop
