@extends('layouts.dashboard')

@section('css')
@endsection

@section('breadcrumbs')
{!! Layouts::breadcrumbs(['type' => $isEdit ? 'edit' : 'create', 'module' => $isEdit ? $institute : null, 'route' => 'institutes'], true) !!}
@endsection

@section('content')
<section class="content">
    <h4>{{ $isEdit ? "EDIT" : "CREATE NEW" }} <span class="text-uppercase">@choice('institutes.title', 1)</span></h4>
    <div class="row">
        <div class="col-xs-12">
            @if ($isEdit)
                {!! Form::model($institute, ['route' => ['institutes.update', $institute->id], 'method' => 'put']) !!}
                {!! Form::hidden('isEdit', true) !!}
            @else
                {!! Form::open(['route' => ['institutes.store'], 'method' => 'post']) !!}
            @endif
            <div class=x_panel>
                <div class=x_title>
                    <strong>@choice('institutes.title', 1) Properties</strong>
                    <small>Form</small>
                </div>
                <div class=x_content>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                {!! Form::label('', 'Institute Type', ['class' => 'form-label']) !!}
                                {!! Form::select('type', \App\Institute::getCookedTypes(), null, ['class' => 'form-control select2', $isEdit ? 'disabled' : '']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('', 'Institute Title', ['class' => 'form-label']) !!}
                                {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => "Badan Eksekutif"]) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('', "Institute Name (Cabinet Name)", ['class' => 'form-label']) !!}
                                {!! Form::text('name', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-dot-circle-o"></i> Submit</button>
                    <button type="reset" class="btn btn-sm btn-danger"><i class="fa fa-ban"></i> Reset</button>
                    @if ($isEdit)
                        <a href="{{ route('institutes.show', $institute->id) }}" type="button" class="btn btn-sm btn-secondary"><i class="fa fa-arrow-left"></i> Back</a>
                    @endif
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</section>
@endsection

@section('js')
@endsection
