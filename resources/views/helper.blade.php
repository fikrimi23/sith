<script type="text/javascript">
    function datatablesLanguage() {
        return {
            "lengthMenu": "@lang('datatables.lengthMenu')",
            "zeroRecords": "@lang('datatables.zeroRecords')",
            "info": "@lang('datatables.info')",
            "infoEmpty": "@lang('datatables.infoEmpty')",
            "infoFiltered": "@lang('datatables.infoFiltered')",
            "emptyTable": "@lang('datatables.emptyTable')",
            "loadingRecords": "@lang('datatables.loadingRecords')",
            "processing": "@lang('datatables.processing')",
            "search": "@lang('datatables.search')",
            "paginate": {
                "first": "@lang('datatables.paginate.first')",
                "last": "@lang('datatables.paginate.last')",
                "next": "@lang('datatables.paginate.next')",
                "previous": "@lang('datatables.paginate.previous')"
            }
        }
    }
</script>
