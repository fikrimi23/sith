@extends('layouts.dashboard')

@section('css')
@endsection

@section('breadcrumbs')
    @php
        $breadCrumbSettings = [
            'Institutes' => route('institutes.index'),
            getInitials($institute->title)." ".$institute->name => route('institutes.show', $institute->show),
        ];
    @endphp
    {!! Layouts::breadcrumbs($breadCrumbSettings) !!}
@endsection

@section('content')
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class=x_panel>
                <div class=x_content>
                    <div class="row">
                        <div class="col-xs-10">
                            <p class="m-b-xs">{{ $institute->title }}</p>
                            <h3 class="text-uppercase">{{ $institute->name }}</h3>
                        </div>
                        <div class="col-xs-2">
                            {!! Form::open(['url' => route('institutes.destroy', $institute->id), 'method'=>'delete' ,]) !!}
                                <a href="{{ route('institutes.edit', $institute->id) }}" class="btn btn-block btn-info"><i class="fa fa-fw fa-edit"></i> Edit</a>
                                <a href="#" class="btn btn-block btn-danger js-delete-confirm"><i class="fa fa-fw fa-trash"></i> Delete</a>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
@endsection
