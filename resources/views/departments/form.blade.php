@extends('layouts.dashboard')

@section('css')
{!! Layouts::usePlugin('select2', 'css') !!}
@endsection

@section('breadcrumbs')
    <li class="breadcrumb-item"><a href="{{ route('institutes.index') }}">@choice('institutes.title', 1)</a></li>
    <li class="breadcrumb-item"><a href="{{ route('institutes.show', $department->institute->id) }}">{{ getInitials($department->institute->title)." ".$department->institute->name }}</a></li>
    @if ($isEdit)
        <li class="breadcrumb-item">{{ $department->alias }}</li>
    @else
        <li class="breadcrumb-item">New @choice('departments.title', 1)</li>
    @endif
@endsection

@section('content')
<section class="content">
    <div class="row m-b-sm">
        <div class="col-xs-6">
            <h4>
                {{ $isEdit ? "EDIT" : "CREATE NEW" }} <span class="text-uppercase">@choice('departments.title', 1)</span>
            </h4>
        </div>
        @if ($isEdit)
        @can('delete', $department)
            <div class="col-xs-6">
                {!! Form::open(['url' => route('departments.destroy', $department->id), 'method'=>'delete' ,]) !!}
                    <a
                        href="#"
                        data-confirm="Are you sure to delete this resources?"
                        class="btn btn-danger btn-md pull-right js-delete-confirm"
                        ><i class="fa fa-fw fa-trash-o"></i> Remove
                        </a>
                {!! Form::close() !!}
            </div>
        @endcan
        @endif
    </div>
    <div class="row">
        <div class="col-xs-12">
            @if ($isEdit)
                {!! Form::model($department, ['route' => ['departments.update', $department->id], 'method' => 'put']) !!}
            @else
                {!! Form::model($department, ['route' => ['departments.store'], 'method' => 'post']) !!}
            @endif
            {!! Form::hidden('isEdit', $isEdit) !!}
            <div class=x_panel>
                <div class=x_title>
                    <strong>@choice('departments.title', 1) Properties</strong>
                    <small>Form</small>
                </div>
                <div class=x_content>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                {!! Form::label('', 'Institute', ['class' => 'form-label']) !!}
                                {!! Form::select('institute[id]', $institutes, null, ['class' => 'form-control select2', 'disabled']) !!}
                                {!! Form::hidden('institute[id]', $department->institute->id) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('', 'Department Title', ['class' => 'form-label']) !!}
                                {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => "Departemen Hubungan Masyarakat"]) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('', 'Departement Alias', ['class' => 'form-label']) !!}
                                {!! Form::text('alias', null, ['class' => 'form-control', 'placeholder' => "Dept Humas"]) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('', 'Description', ['class' => 'form-label']) !!}
                                {!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => "Humas bertugas ..."]) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('', 'Visi', ['class' => 'form-label']) !!}
                                {!! Form::textarea('vision', null, ['class' => 'form-control', 'placeholder' => "Menjadikan ..."]) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('', 'Misi', ['class' => 'form-label']) !!}
                                {!! Form::textarea('mission', null, ['class' => 'form-control', 'placeholder' => "1. ..."]) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-dot-circle-o"></i> Submit</button>
                    <button type="reset" class="btn btn-sm btn-danger"><i class="fa fa-ban"></i> Reset</button>
                    @if ($isEdit)
                        <a href="{{ url()->previous() }}" type="button" class="btn btn-sm btn-secondary"><i class="fa fa-arrow-left"></i> Back</a>
                    @endif
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</section>
@endsection

@push('js')
{!! Layouts::usePlugin('select2', 'js') !!}
@endpush
