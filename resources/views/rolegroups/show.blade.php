@extends('layouts.dashboard')

@section('css')
@endsection

@section('content')
<section class="content">
    <h4>MODULE</h4>
    <div class="row">
        <div class="col-xs-12 col-md-6">
            {!! Form::model($module, ['route' => ['modules.update', $module->id], 'method' => 'put']) !!}
            <div class=x_panel>
                <div class=x_title>
                    <strong>Module Properties</strong>
                    <small>Form</small>
                </div>
                <div class=x_content>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group form-float">
                                {!! Form::label('', 'Module name', ['class' => 'form-label']) !!}
                                {!! Form::text('module_name', null, ['class' => 'form-control', 'disabled', 'placeholder' => 'Module Name']) !!}
                            </div>
                            <div class="form-group form-float">
                                {!! Form::label('', "Module Alias", ['class' => 'form-label']) !!}
                                {!! Form::text('module_alias', null, ['class' => 'form-control', 'disabled', 'placeholder' => 'modulealias']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</section>
@endsection

@push('js')
@endpush
