@foreach ($menu as $key => $nav)
    @if ($nav['authorized'] ?? true)
        @php
            $hasSubnav  = isset($nav['subnav']);
            $isSmall    = $nav['small'] ?? false;
        @endphp
        <li class="{{ $nav['active'] && $hasSubnav ? 'active' : ($nav['active'] ? 'current-page' : '') }}">
            <a href="{{ $nav['url'] }}">
                <i class="fa {{ $nav['icon'] }}"></i>
                {{-- check if small --}}
                <span {{ ($isSmall ? 'class="text-xss"' : '') }}>{{ $nav['title'] }}</span>

                @if ($hasSubnav)
                    <span class="fa fa-chevron-down"></span>
                @endif
            </a>
            {{-- check if has sub nav --}}
            @if ($hasSubnav)
                <ul class="nav child_menu">
                @foreach ($nav['subnav'] as $subnav)
                    @if ($subnav['authorized'] ?? true)
                        <li  class="{{ $subnav['active'] ? 'current-page' : ''}}">
                            <a href="{{ $subnav['url'] }}">
                                {{-- <i class="fa {{ $subnav['icon'] ?? '' }}"></i> --}}
                                <span>{{ $subnav['title'] }}</span>
                            </a>
                        </li>
                    @endif
                @endforeach
                </ul>
            @endif
        </li>
    @endif
@endforeach
