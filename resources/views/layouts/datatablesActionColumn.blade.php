{{-- Button if url view is exist --}}
@if (isset($view))
<a class="btn btn-primary btn-xs" href="{{ $view }}" data-toggle="tooltip" data-placement="bottom" title="View">
    <i class="fa fa-lg fa-folder"></i> View
</a>
@endif

{{-- Button if url edit is exist --}}
@if (isset($edit))
<a class="btn btn-info btn-xs" href="{{ $edit }}" data-toggle="tooltip" data-placement="bottom" title="Edit">
    <i class="fa fa-lg fa-pencil"></i> Edit
</a>
@endif

{{-- Button if url delete is exist --}}
@if (isset($delete))
{!! Form::open([ 'url' => $delete, 'method'=>'delete', 'class'=>'form-inline','style="display:inline;"' ]) !!}
    <a href="#" data-confirm="Are you sure to delete this resources?" class="btn btn-danger btn-xs js-delete-confirm" data-toggle="tooltip" data-placement="bottom" title="Delete">
        <i class="fa fa-lg fa-trash-o"></i> Delete
    </a>
{!! Form::close() !!}
@endif
