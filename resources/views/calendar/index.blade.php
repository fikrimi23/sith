<!-- TODO Documentation -->
@extends('layouts.dashboard')

@section('css')
{!! Layouts::usePlugin('fullcalendar', 'css') !!}
{!! Layouts::usePlugin('pnotify', 'css') !!}
{!! Layouts::usePlugin('select2', 'css') !!}
@endsection

@section('breadcrumbs')
{!! Layouts::breadcrumbs(['type' => 'index', 'route' => 'calendar'], true) !!}
@endsection

@section('content')
<style type="text/css">
    .x_content > .fc-event {
        padding: 5px 10px;
        margin: 5px;
        cursor: pointer;

    }
</style>
<section class="content">
    <div class="row m-b-sm">
        <div class="col-xs-12">
            <h3>Events Calendar <small>Click to add/edit events</small></h3>
        </div>
    </div>

    <div class="fadeIn">
        <div class="row">
            <div class="col-xs-2">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="x_panel" id="trash-box">
                            <h2>Trash Box <i class="fa fa-trash"></i></h2>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="x_panel" id="external-events">
                            <div class="x_title">
                                <h2>Unassigned</h2>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content" id="external-events-listing">
                                @foreach ($unassignedEvents as $event)
                                    <div class="fc-event draggable" data-id="{{ $event->id }}">{{ $event->title }}</div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-10">
                <div class=x_panel>
                    <div class=x_content>
                    <div id='calendar' data-urledit="{{ route('events.store') }}" data-url="{{ route('events.index') }}"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div id="calendarModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="antoform" class="form-horizontal calender" role="form">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">New Calendar Entry</h4>
            </div>
            <div class="modal-body">
                <div id="testmodal" style="padding: 5px 20px;">
                    <div class="form-group">
                        {!! Form::label('institute_id', 'Institute', ['class' => 'col-sm-3 control-label']) !!}
                        <div class="col-sm-9">
                            {!! Form::select('institute_id', $institutes, null, ['class' => 'form-control select2', 'style' => 'width:100%']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('title', 'Department', ['class' => 'col-sm-3 control-label']) !!}
                        <div class="col-sm-9">
                            {!! Form::select('department_id', [], null, ['class' => 'form-control', 'style' => 'width:100%', 'data-url' => route('departments.index')]) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('title', 'Title', ['class' => 'col-sm-3 control-label']) !!}
                        <div class="col-sm-9">
                            {!! Form::text('title', null, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default antoclose" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary antosubmit">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>

@endsection

@push('js')
{!! Layouts::usePlugin('jqueryui', 'js') !!}
{!! Layouts::usePlugin('moment', 'js') !!}
{!! Layouts::usePlugin('fullcalendar', 'js') !!}
{!! Layouts::usePlugin('pnotify', 'js') !!}
{!! Layouts::usePlugin('select2', 'js') !!}

<script>
    $(document).ready(function() {
        function init_calendar() {
           var
                date = new Date(),
                d = date.getDate(),
                m = date.getMonth(),
                y = date.getFullYear(),
                started,
                categoryClass,
                calendar = $('#calendar'),
                editUrl = calendar.data('urledit');

            triggerDraggable();

            $.ajaxSetup({
                url: editUrl,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                }
            });

            $(document).ajaxStart(function() {
                NProgress.configure({
                    showSpinner: false,
                });
                NProgress.start();
            });

            $(document).ajaxStop(function() {
                NProgress.done();
            });

            $('form').keypress(function (e) {
                if (e.which == 13) {
                    $('.antosubmit').click();
                    return false;    //<---- Add this line
                }
            });

            /* initialize the external events
            -----------------------------------------------------------------*/

            $('#external-events .fc-event').each(function() {

                // store data so the calendar knows to render an event upon drop
                $(this).data('event', {
                    id: $(this).data('id'),
                    title: $.trim($(this).text()), // use the element's text as the event title
                    stick: true // maintain when user navigates (see docs on the renderEvent method)
                });
            });

            /* initialize the calendar
            -----------------------------------------------------------------*/

            calendar.fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,listMonth'
                },
                events: calendar.data('url'),
                selectable: true,
                weekNumbers: true,
                // unselectAuto: false,
                editable: true,
                droppable: true,
                dragRevertDuration: 0,

                firstDay: 1,
                drop: function(date, jsEvent, ui, resourceId) {
                    $(this).remove();

                    $.ajax({
                        url: editUrl+"/"+$(this).data('id'),
                        type: 'PUT',
                        data: {
                            'start': stringifyDate(date),
                            'end': stringifyDate(date.add(1, 'day'))
                        },
                    })
                    .done(function(data, textStatus, xhr) {
                        // renderEvent(data);
                    })
                },
                eventDrop: function(event, delta, revertFunc, jsEvent, ui, view) {
                    $.ajax({
                        url: editUrl+"/"+event.id,
                        type: 'PUT',
                        data: {
                            'title': event.title,
                            'start': stringifyDate(event.start),
                            'end': stringifyDate(event.end)
                        },
                    })
                    .fail(function () {
                        revertFunc();
                    })
                },
                eventDragStop: function(event, jsEvent, ui, view) {
                    tempEvent = event;

                    elem = document.elementFromPoint(jsEvent.clientX, jsEvent.clientY);
                    boxId = $(elem).closest('.x_panel').attr('id');

                    if (boxId == 'external-events') {
                        calendar.fullCalendar('removeEvents', event._id);
                        eventDiv = $("<div class='fc-event draggable' data-id='"+event.id+"'>").appendTo('#external-events-listing').text(event.title);

                        eventDiv.data('event', {
                            id: event.id,
                            title: event.title, // use the element's text as the event title
                            stick: true // maintain when user navigates (see docs on the renderEvent method)
                        });
                        triggerDraggable();

                        $.ajax({
                            url: editUrl+"/"+event.id,
                            type: 'PUT',
                            data: {
                                'title': event.title,
                                'start': null,
                                'end': null
                            },
                        })
                    } else if (boxId == 'trash-box') {
                        calendar.fullCalendar('removeEvents', event.id);
                        $.ajax({
                            url: editUrl+"/"+event.id,
                            type: 'DELETE',
                        })
                        .done(function () {
                            PNotify.prototype.options.delay = 500;
                            var
                                notice = new PNotify({
                                    type: "danger",
                                    text: "Event has been deleted",
                                    // addclass: 'dark',
                                    styling: 'bootstrap3',
                                    // hide: false,
                                    buttons: {
                                        sticker: false
                                    }
                                });
                            notice.get().click(function() {
                                notice.remove();
                            });
                        })
                    }
                },
                select: function(start, end, allDay) {
                    $('#calendarModal').modal('toggle');
                    started = start,
                    ended = end;

                    $('.antosubmit').unbind('click').click(function(event) {
                        event.preventDefault()
                        var
                            title = $("#title").val();
                            department_id = $("select[name=department_id]").val();

                        categoryClass = $("#event_type").val();
                        if (title) {
                            $.ajax({
                                url: calendar.data('urlcreate'),
                                type: 'POST',
                                data: {
                                    'department_id': department_id,
                                    'title': title,
                                    'start': stringifyDate(start),
                                    'end': stringifyDate(end)
                                },
                            })
                            .done(function(data, textStatus, xhr) {
                                renderEvent(data);
                            })
                        }

                        $('#calendarModal').modal('toggle');
                    });
                },
                eventClick: function(calEvent, jsEvent, view) {
                    $('#calendarModal').modal('toggle');
                    $('#title').val(calEvent.title);
                    $('#id').val(calEvent.id);
                    categoryClass = $("#event_type").val();

                    $('.antosubmit').unbind('click').click(function(event) {
                        event.preventDefault()
                        calEvent.title = $("#title").val();
                        if (title) {
                            updateEvent(calEvent);
                        }
                        $('#calendarModal').modal('toggle');
                    });
                }

            });

            $('#calendarModal').on('hidden.bs.modal', function (e) {
                $('#title').val('');
                calendar.fullCalendar('unselect');
            })

            var isEventOverDiv = function(x, y, type = null) {
                var box = null;
                if (type == 'unassigned') {
                    box = $('#external-events')
                } else if (type == 'trash') {
                    box = $('#trash-box')
                } else {
                    return false;
                }

                var offset = box.offset();
                offset.right = box.width() + offset.left;
                offset.bottom = box.height() + offset.top;

                // Compare
                if (x >= offset.left
                    && y >= offset.top
                    && x <= offset.right
                    && y <= offset .bottom) { return true; }
                return false;
            }

            function renderEvent(data) {
                calendar.fullCalendar('renderEvent', {
                    id: data.id,
                    title: data.title,
                    start: data.start,
                    end: data.end,
                    allDay: true
                }, true);
            }

            function stringifyDate(date) {
                if (typeof date !== undefined && date !== null) {
                    return date.format('YYYY-MM-DD')
                } else {
                    return date;
                }
            }

            function triggerDraggable() {
                $(".draggable").draggable({
                    zIndex: 999,
                    revert: true,      // will cause the event to go back to its
                    revertDuration: 0  //  original position after the drag
                });
            }
        }

        init_calendar();

        function refresh_department() {
            select2 = $('select[name=department_id]');
            console.log(select2.data('url'));
            $.get(select2.data('url'), {'action':'get_all', 'institute_id': $('select[name=institute_id]').val()}, function(data) {
                if (data.length > 0) {
                    $.each(data, function(index, val) {
                        select2.append($('<option/>', {
                            value: val.id,
                            text: val.title
                        }));
                    });
                }
                select2.select2();
            });

        }

        refresh_department();

        $('select[name=department_id]').on('change', function(event) {
            event.preventDefault();
            refresh_department();
        });
    });
</script>
@endpush
