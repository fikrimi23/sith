@extends('layouts.dashboard')

@section('css')
@endsection

@section('content')
<section class="content">
    <h4>MODULE</h4>
    <div class="row">
        <div class="col-xs-12 col-md-6">
            <div class=x_panel>
                <div class=x_title>
                    <strong>Module Properties</strong>
                    <small>Form</small>
                </div>
                <div class=x_content>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                {!! Form::label('', 'Module name', ['class' => 'form-label']) !!}
                                <p><strong><big>{{ $module->module_name }}</big></strong></p>
                            </div>
                            <div class="form-group">
                                {!! Form::label('', "Module Alias", ['class' => 'form-label']) !!}
                                <p><strong><big>{{ $module->module_alias }}</big></strong></p>
                            </div>
                            <div class="form-group">
                                @if ($module->locked)
                                    <span class="badge badge-danger">locked</span>
                                @else
                                    <span class="badge badge-success">unlocked</span>
                                @endif
                                @if ($module->module_core)
                                    <span class="badge badge-success">core</span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
@endsection
