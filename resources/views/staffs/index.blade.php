@extends('layouts.dashboard')

@section('css')
<link rel="stylesheet" href="{{ asset('vendor/datatables/datatables.min.css') }}" type="text/css"/>
<link rel="stylesheet" href="{{ asset('vendor/datatables/DataTables-1.10.13/css/dataTables.bootstrap4.min.css') }}" type="text/css"/>
@endsection

@section('content')
{{-- <section class="content"> --}}
<div class="row">
    <div class="col-xs-12">
        <h4>
            <span class="text-uppercase">@lang('staffs.title')</span>
            <a href="{{ route('staffs.create') }}" class="btn btn-primary btn-sm pull-right">
                <i class="fa fa-fw icon-plus"></i> New @lang('title')
            </a>
        </h4>
    </div>
</div>

<div class="animated fadeIn">
    <div class="row">
        <div class="col-xs-12">
            <div class=x_panel>
                <div class=x_content>
                    <table class="table table-sm" id="staffs-table" data-url="{{ route('staffs.datatables') }}" data-token="{{ csrf_token() }}">
                        <thead class="thead-inverse">
                            <tr>
                                <th>@lang('staffs.table.email')</th>
                                <th>@lang('staffs.table.active')</th>
                                <th>@lang('staffs.table.action')</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- </section> --}}
@endsection

@section('js')
<script src="{{ asset('vendor/bootbox/bootbox.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/DataTables-1.10.13/js/dataTables.bootstrap4.min.js') }}"></script>
<script>
    $(document).ready(function() {
        $('#staffs-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: $('#staffs-table').data('url'),
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('#staffs-table').data('token')
                }
            },
            columns: [
                {data: 'email', name: 'Name'},
                {data: 'is_active', name: 'Alias'},
                {data: 'action', name: 'Action', sortable: false, searchable: false},
            ],
            language: datatablesLanguage()
          });
    });
</script>
@endsection
