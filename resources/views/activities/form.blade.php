@extends('layouts.dashboard')

@section('css')
{!! Layouts::usePlugin('select2', 'css') !!}
@endsection

@section('breadcrumbs')
{!! Layouts::breadcrumbs(['type' => $isEdit ? 'edit' : 'create', 'module' => $isEdit ? $activity : null, 'route' => 'activities'], true) !!}
@endsection

@section('content')
<section class="content">
    <h4>{{ $isEdit ? "EDIT" : "CREATE NEW" }} ACTIVITY</h4>
    <div class="row">
        <div class="col-xs-12">
            @if ($isEdit)
                {!! Form::model($activity, ['route' => ['activities.update', $activity->id], 'method' => 'put']) !!}
                {!! Form::hidden('isEdit', true) !!}
            @else
                {!! Form::open(['route' => ['activities.store'], 'method' => 'post']) !!}
            @endif
            <div class=x_panel>
                <div class=x_title>
                    <strong>Activity Properties</strong>
                    <small>Form</small>
                </div>
                <div class=x_content>
                    <div class="row">
                        <div class="col-xs-12">
                            @if (! $isEdit)
                                <div class="form-group">
                                    {!! Form::label('', 'Institute', ['class' => 'form-label']) !!}
                                    {!! Form::select('institute[id]', $institutes, null, ['class' => 'form-control select2' , 'data-department-url' => route('departments.index')]) !!}
                                    @if ($isEdit)
                                        {!! Form::hidden('institute[id]', $activity->department->institute->id) !!}
                                    @endif
                                </div>
                            @endif
                            <div class="form-group">
                                {!! Form::label('', 'Department', ['class' => 'form-label']) !!}
                                {!! Form::select('department[id]', $isEdit ? $departments : [], null, ['class' => 'form-control select2', 'disabled']) !!}
                                @if ($isEdit)
                                    {!! Form::hidden('department[id]', $activity->department->id) !!}
                                @endif
                            </div>
                            <div class="form-group form-float">
                                {!! Form::label('', 'Activity title', ['class' => 'form-label']) !!}
                                {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Describe activity title']) !!}
                            </div>
                            <div class="form-group form-float">
                                {!! Form::label('', "Activity description", ['class' => 'form-label']) !!}
                                {!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Describe activity description in longer text']) !!}
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    {!! Form::hidden('is_event', 0) !!}
                                    {!! Form::checkbox('is_event', true, null, ['class' => 'form-check-input']) !!}
                                    &nbsp;Is Event
                                </label>
                            </div>
{{--                             <div class="form-check">
                                <label class="form-check-label">
                                    {!! Form::hidden('locked', 0) !!}
                                    {!! Form::checkbox('locked', true, null, ['class' => 'form-check-input']) !!}
                                    &nbsp;Is Programs
                                </label>
                            </div> --}}
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-dot-circle-o"></i> Submit</button>
                    <button type="reset" class="btn btn-sm btn-danger"><i class="fa fa-ban"></i> Reset</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</section>
@endsection

@push('js')
{!! Layouts::usePlugin('select2', 'js') !!}
{{-- {!! Layouts::usePlugin('parsley', 'js') !!} --}}
<script type="text/javascript">
    $(document).ready(function() {
        $('select[name="institute[id]"]').change(function() {
            url = $(this).data('department-url');
            institute_id = $(this).val();
            data = {
                'institute_id': institute_id,
                'action': 'institutes_dropdown'
            }
            $.get(url, data, function(data, textStatus, xhr) {
                selectDept = $('select[name="department[id]"]');
                selectDept.empty();
                $.each(data, function(id, title) {
                    selectDept.append($("<option></option>")
                       .attr("value", id).text(title));
                });
                selectDept.removeAttr('disabled');
                selectDept.select2();
            });
        });
    });
</script>
@endpush
