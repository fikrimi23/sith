@extends('layouts.dashboard')

@section('css')
@endsection

@section('breadcrumbs')
    {{-- <li class="breadcrumb-item"><a href="{{ route('institutes.index') }}">Institute</a></li> --}}
    {{-- <li class="breadcrumb-item"><a href="{{ route('institutes.show', $institute->id) }}">{{ getInitials($institute->title)." ".$institute->name }}</a></li> --}}
@endsection

@section('content')
<section class="content">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Activity Detail <small> {{ $activity->title }}</small></h3>
            </div>

            {{-- <div class="title_right"> --}}
                {{-- <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search"> --}}
                    {{-- <div class="input-group"> --}}
                        {{-- <input type="text" class="form-control" placeholder="Search for..."> --}}
                        {{-- <span class="input-group-btn"> --}}
                            {{-- <button class="btn btn-default" type="button">Go!</button> --}}
                        {{-- </span> --}}
                    {{-- </div> --}}
                {{-- </div> --}}
            {{-- </div> --}}
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Overview</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            {{-- <li class="dropdown"> --}}
                                {{-- <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a> --}}
                                {{-- <ul class="dropdown-menu" role="menu"> --}}
                                    {{-- <li><a href="#">Settings 1</a> --}}
                                    {{-- </li> --}}
                                    {{-- <li><a href="#">Settings 2</a> --}}
                                    {{-- </li> --}}
                                {{-- </ul> --}}
                            {{-- </li> --}}
                            {{-- <li><a class="close-link"><i class="fa fa-close"></i></a> --}}
                            {{-- </li> --}}
                        </ul>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content">

                        <div class="col-md-9 col-sm-9 col-xs-12">

                            <ul class="stats-overview">
                                <li>
                                    <span class="name"> Estimated budget </span>
                                    <span class="value text-success"> 2300 </span>
                                </li>
                                <li>
                                    <span class="name"> Total amount spent </span>
                                    <span class="value text-success"> 2000 </span>
                                </li>
                                <li class="hidden-phone">
                                    <span class="name"> Estimated project duration </span>
                                    <span class="value text-success"> 20 </span>
                                </li>
                            </ul>
                            <br />

                            <div id="mainb" style="height:350px;"></div>

                            <div>

                                <h4>Recent Activity
                                    <span class="pull-right"><button data-method="POST" data-url="{{ route('activitylogs.store') }}" data-title="" data-date="{{ \Carbon\Carbon::now()->format('Y-m-d') }}" class="btn btn-xs btn-success activitylog-edit" style="margin-right: 0"><i class="fa fa-plus"></i> Add</button></span>
                                </h4>
                                <!-- end of user messages -->
                                <ul class="messages">
                                    @foreach ($activity->logs as $log)
                                    <li>
                                        <img src="{{ asset('images/img.jpg') }}" class="avatar" alt="Avatar">
                                        <div class="message_date">
                                            {!! Form::open([ 'url' => route('activitylogs.destroy', $log->id), 'method'=>'delete', 'class'=>'form-inline','style="display:inline;"' ]) !!}
                                            <div class="btn-group">
                                                <button data-method="PUT" data-url="{{ route('activitylogs.update', $log->id) }}" data-title="{{ $log->title }}" data-date="{{ $log->date->format('Y-m-d') }}" class="activitylog-edit btn btn-xs btn-warning" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-pencil"></i></button>
                                                <button href="#" data-confirm="Are you sure to delete this resources?" class="btn btn-danger btn-xs js-delete-confirm" data-toggle="tooltip" data-placement="bottom" title="Delete">
                                                    <i class="fa fa-trash-o"></i>
                                                </button>
                                            </div>
                                            {!! Form::close() !!}
                                            <h3 class="date text-info">{{ $log->date->format('d') }}</h3>
                                            <p class="month">{{ $log->date->format('M') }}</p>
                                        </div>
                                        <div class="message_wrapper">
                                            <h4 class="heading">{{ $log->user->email }}</h4>
                                            <blockquote class="message">{{ $log->title }}</blockquote>
                                            <br />
                                            <p class="url">
                                                <span class="fs1 text-info" aria-hidden="true" data-icon=""></span>
                                                <a href="#"><i class="fa fa-paperclip"></i> User Acceptance Test.doc </a>
                                            </p>
                                        </div>
                                    </li>
                                    @endforeach
                                </ul>
                                <!-- end of user messages -->


                            </div>


                        </div>

                        <!-- start project-detail sidebar -->
                        <div class="col-md-3 col-sm-3 col-xs-12">
                            <section class="panel">
                                <div class="x_title">
                                    <h2>Activity Description</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="panel-body">
                                    <h3 class="green"><i class="fa fa-bolt"></i> {{ $activity->title }}</h3>

                                    <p>{{ $activity->description }}</p>
                                    <br />

                                    <div class="project_detail">
                                        <p class="title"><i class="fa fa-sitemap"></i> Institute</p>
                                        <p>{{ $activity->department->institute->full_title }} <a href="{{ route('institutes.show', $activity->department->institute->id) }}"><i class="fa fa-external-link"></i></a></p>
                                        <p class="title"><i class="fa fa-sitemap"></i> Department</p>
                                        <p>{{ $activity->department->title }} <a href="{{ route('departments.show', $activity->department->id) }}"><i class="fa fa-external-link"></i></a></p>
                                    </div>

                                    <br />
{{--                                     <h5>Project files</h5>
                                    <ul class="list-unstyled project_files">
                                        <li><a href=""><i class="fa fa-file-word-o"></i> Functional-requirements.docx</a>
                                        </li>
                                        <li><a href=""><i class="fa fa-file-pdf-o"></i> UAT.pdf</a>
                                        </li>
                                        <li><a href=""><i class="fa fa-mail-forward"></i> Email-from-flatbal.mln</a>
                                        </li>
                                        <li><a href=""><i class="fa fa-picture-o"></i> Logo.png</a>
                                        </li>
                                        <li><a href=""><i class="fa fa-file-word-o"></i> Contract-10_12_2014.docx</a>
                                        </li>
                                    </ul>
                                    <br />

                                    <div class="text-center mtop20">
                                        <a href="#" class="btn btn-sm btn-primary">Add files</a>
                                        <a href="#" class="btn btn-sm btn-warning">Report contact</a>
                                    </div> --}}
                                </div>

                            </section>

                        </div>
                        <!-- end project-detail sidebar -->

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div id="modal-activitylog" class="modal center fade" tabindex="-1" role="dialog" aria-labelledby="modal-activitylog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div style="padding: 5px 20px;">
                    {!! Form::open(['id' => 'form-activitylog', 'class' => 'form-horizontal']) !!}
                        {!! Form::hidden('activity.id', $activity->id) !!}
                        {!! Form::hidden('user.id', auth()->id()) !!}
                        {{ method_field('POST') }}
                        <div class="form-group">
                            {!! Form::label('title', 'Log', ['class' => 'col-sm-1 control-label']) !!}
                            <div class="col-sm-3">
                                {!! Form::date('date', \Carbon\Carbon::now(), ['class' => 'form-control']) !!}
                            </div>
                            <div class="col-sm-6">
                                {!! Form::text('title', null, ['class' => 'form-control']) !!}
                            </div>
                            <div class="col-sm-2">
                                <button type="submit" class="pull-right btn btn-success"><i class="fa fa-check"></i></button>
                                <button type="button" class="pull-right btn btn-danger" data-dismiss="modal"><i class="fa fa-close"></i></button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script type="text/javascript">
    $('#modal-activitylog').on('shown.bs.modal', function(event) {
        $('input[name="title"]').focus();
    });
    $(".activitylog-edit").click(function(event) {
        event.preventDefault();
        var
            date = $(this).data('date');
            title = $(this).data('title');
            url = $(this).data('url');
            method = $(this).data('method');

            console.log(date);
            console.log(title);

        $('#form-activitylog').attr({
            action: url,
        });;

        $('#form-activitylog').find('input[name="date"]').val(date);
        $('#form-activitylog').find('input[name="title"]').val(title);
        $('#form-activitylog').find('input[name="_method"]').val(method);

        $('#modal-activitylog').modal('show');
    });
</script>
@endpush
