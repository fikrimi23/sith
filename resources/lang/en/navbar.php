<?php

return [
    'profile'   => 'Profile',
    'logout'    => 'Sign Out',

    'title'     => 'Main Navigation',

    'dashboard' => 'Dashboard',

    // Module-based language

    'users'      => [
        'title'      => "Users",
        'staffs'     => trans_choice('Staffs.title', 2),
        'users'      => trans_choice('Users.title', 2),
        'rolegroups' => trans_choice('Rolegroups.title', 2),
    ],

    'activities'         => trans_choice('activities.title', 2),
    'calendar'       => trans('calendar.title'),

    'organizations' => [
        'title'       => "Organizations",
        'institutes'  => trans_choice('institutes.title', 2),
        'departments' => trans_choice('departments.title', 2),
    ],

    'settings'         => [
        'title' => 'Settings',
        'modules' => trans_choice('modules.title', 2)
    ],
];
